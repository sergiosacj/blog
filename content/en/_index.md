## What is this blog about?

While I'm practicing for programming competitions I learn a bunch of cool stuff. But, learning without sharing is pretty
boring. So I created this blog to share what I'm studying, and bonus, I can practice my english writing.
